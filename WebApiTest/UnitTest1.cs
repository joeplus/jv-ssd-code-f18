using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Controllers;
using WebApi.Models;

namespace WebApiTest
{
    [TestClass]
    public class UnitTest1
    {
        private static DealershipContext dealershipContext = null;

        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            // initialize our db context
            var options = new DbContextOptionsBuilder<DealershipContext>()
                            .UseInMemoryDatabase(databaseName: "DealershipTestDB").Options;
            dealershipContext = new DealershipContext(options);

            // add records to context and save
            Car car1 = new Car { Id = 1, Make = "Toyota", Model = "Camry", Year = 2017 };
            Car car2 = new Car { Id = 2, Make = "Honda", Model = "Accord", Year = 2018 };
            dealershipContext.Cars.AddRange(car1, car2);
            dealershipContext.SaveChanges();
        }

        [TestMethod]
        public async Task CarApiGetAllTest()
        {
            //Arrange
            CarApiController carApi = new CarApiController(dealershipContext);

            //Act
            IEnumerable<Car> theCars = carApi.GetCar();

            //Assert
            Assert.IsInstanceOfType(theCars, typeof(DbSet<Car>));
            DbSet<Car> dbSetCars = theCars as DbSet<Car>;
            int count = await dbSetCars.CountAsync();
            Assert.AreEqual(2, count);

            Car car1 = dbSetCars.Find(1);
            Assert.AreEqual("Toyota", car1.Make);
            Assert.AreEqual("Camry", car1.Model);

            Car car2 = dbSetCars.Find(2);
            Assert.AreEqual(2, car2.Id);
            Assert.AreEqual(2018, car2.Year);
        }
    }
}
