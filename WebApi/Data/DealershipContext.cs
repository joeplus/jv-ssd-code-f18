﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Models
{
    public class DealershipContext : DbContext
    {
        public DealershipContext (DbContextOptions<DealershipContext> options)
            : base(options)
        {
        }

        public DbSet<WebApi.Models.Car> Cars { get; set; }
    }
}
